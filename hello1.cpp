///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07c - My First Cat - EE 205 - Spr 2022
///
/// Usage: ./hello1
///
/// Result: Hello World
///
/// @file hello1.cpp
/// @version 1.0
///
/// @author Reid Lum <reidlum@hawaii.edu>
/// @date   21 Feb 2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
using namespace std;
 
int main()
{
    cout << "Hello World" << endl;
 
    return 0;
}
