///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07c - My First Cat - EE 205 - Spr 2022
///
/// Usage: ./hello2
///
/// Result: Hello World
///
/// @file hello2.cpp
/// @version 1.0
///
/// @author Reid Lum <reidlum@hawaii.edu>
/// @date   21 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

int main()
{
    std::cout << "Hello World" << std::endl;
 
    return 0;
}

