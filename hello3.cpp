///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07c - My First Cat - EE 205 - Spr 2022
///
/// Usage: ./hello3
///
/// Result: Meow
///
/// @file hello3.cpp
/// @version 1.0
///
/// @author Reid Lum <reidlum@hawaii.edu>
/// @date   21 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

class Cat {
public:
   void sayHello() {
      std::cout << "Meow" << std::endl;
   }
};

int main()
{
   Cat myCat;
   myCat.sayHello();
   return 0;
}

